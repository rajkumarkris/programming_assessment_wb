## Currency Convertor App
### Project
Currency Convertor App

### Description
This App converts the any currency to equivalent Austrlian dollars [AUD].

### Screenshot
![image.png](./Screenshot_20220326_193517_2.png)

### Library
The Currency convertor library has three modules `currencyconvertorlib`, `core` & `repositorycc`. 

#### CurrencyConvertorLib
This Module contains the class for converting the currency from one format to another.

`CurrencyConvertor` -  Currency Convertor class helps converting the currency from one currency format to another format. <br> *Ex: 54.758 INR equals to 1 AUD.*
```kotlin
class CurrencyConvertor(private val rate: Double, private val amount: Double) {
    fun calculateExchangeRate(): Double {
        return (1.div(rate)).times(amount)
    }
}
```
#### Core
The Core Module contains utils classes for the project.

`UIUtils`- Utility class to handle the UI related events. 
```kotlin
object UIUtils {
    fun showSnackBar(view: View, msg: Int) {
        Snackbar.make(
            view,
            msg,
            Snackbar.LENGTH_SHORT
        ).show()
    }
}
```
`NetworkUtil`- Utility class for checking Internet availability.
```kotlin
@ActivityRetainedScoped
class NetworkUtil @Inject constructor(private val context: Context) {

    fun isNetworkConnected(): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val network = connectivityManager.activeNetwork ?: return false
            val activeNetwork = connectivityManager.getNetworkCapabilities(network) ?: return false
            return when {
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            val networkInfo = connectivityManager.activeNetworkInfo ?: return false
            return networkInfo.isConnected
        }
    }
}
```
`Utils`- Utility Class for currency operations.
```kotlin
object Utils {
    fun Double.formatCurrency(): String {
        return DecimalFormat("##.##").apply {
            roundingMode = RoundingMode.CEILING
        }.format(this)
    }

    fun String.isValidDigit(): Boolean {
        return this.toDoubleOrNull()?.let { true } ?: false
    }
}
```
#### RepositoryCC
This module contains classes to handle the backend operations and retrive the list of currencies and rate for different countries.

`Repository` - Repository class for Concurrency Convertor.

```kotlin
@ActivityRetainedScoped
class Repository @Inject constructor(private val dataSource: RemoteDataSource) :
    BaseAPIResponseHandler() {

      suspend fun loadCurrencies(): Flow<APIState<HashMap<String, CurrencyProduct>>> {
        return flow {
            emit(APIState.Loading(true))
            when (val safeApiCall = safeApiCall { dataSource.fetchData() }) {
                is ResponseResult.Success -> {
                    emit(
                        APIState.Success(
                            (safeApiCall.data as CurrencyResponse).data
                                .brands.wbc.portfolios.fx.products
                        )
                    )
                }
                else -> {
                    emit(APIState.Error("APIError"))
                }
            }
            emit(APIState.Loading(false))
        }.flowOn(Dispatchers.IO)
    }
}
```
`RemoteDataSource` - Helper class to manipulate data from Rest backend sever.
```kotlin
class RemoteDataSource @Inject constructor(private val apiService: APIService) : DataSource {
    override suspend fun fetchData(): Response<CurrencyResponse> = apiService.getCurrencyRates()
}
```

# Programming_Assessment_wb

Programming assessment for the WB recruitment process

## Getting started

To get started with the project first clone the code locally. By default the project will be cloned in the Master [Main] branch. Checkout to the develop branch where all the latest features will be available.
```
git clone https://gitlab.com/rajkumarkris/programming_assessment_wb.git
git checkout develop
```
## Project Details
- **IDE** - Android Studio
- **Language** - Kotlin
- **Minimum SDK** - Lollipop [21]
- **Target SDK** - Snow Cone [32]

### Components Used

* Databinding
* MaterialDesign
* ConstraintLayout
* Timber
* ViewModel
* Retrofit
* Gson
* Coroutines
* Hilt
* Flow
* JUnit
* Truth
* Mockito

## Authors
Rajkumar Balakrishnasamy

## License
For open source projects, say how it is licensed.
